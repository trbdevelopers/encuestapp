const api = require("express").Router();
const EncuestaController = require("../controllers/encuesta");

api.get("/encuestas", EncuestaController.getEncuestas);
api.get("/encuestas/:id", EncuestaController.getEncuesta);
api.post("/encuestas/", EncuestaController.createEncuesta);

module.exports = api;