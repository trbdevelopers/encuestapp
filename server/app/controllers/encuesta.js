const Encuesta = require("../models/encuesta");

function getEncuestas(req, res) {
    Encuesta.find({}, (err, encuestas) => {
        if (err)
            return res.status(500).send({
                error: `Ha ocurrido un error en el servidor: ${err}`
            });

        if (!encuestas) {
            res.status(404).send({
                error: `No hay encuestas disponibles`
            });
        }

        return res.status(200).send({
            encuestas
        });
    });
}

function getEncuesta(req, res) {
    const idEncuesta = req.body.id;

    Encuesta.findById(idEncuesta, (err, encuesta) => {
        if (err)
            return res.status(500).send({
                error: `Ha ocurrido un error en el servidor: ${err}`
            });

        if (!encuesta) {
            res.status(404).send({
                error: `No existe ninguna encuesta con ese ID`
            });
        }

        return res.status(200).send({
            encuesta
        });
    });
}

function createEncuesta(req, res) {
    const data = req.body;
    let encuesta = new Encuesta();

    encuesta.id = data.id
    encuesta.titulo = data.titulo
    encuesta.descripcion = data.descripcion
    encuesta.opciones = data.opciones
    encuesta.votos = data.votos

    encuesta.save((err, createdEncuesta) => {
        if (err) {
            res.status(500).send({
                message: `Error al añadir una nueva encuesta ${err}`
            });
        }

        res.status(200).send({
            message: "Encuesta añadida",
            createdEncuesta
        });
    });
}

const API = {
    getEncuestas,
    getEncuesta,
    createEncuesta
};

module.exports = API;