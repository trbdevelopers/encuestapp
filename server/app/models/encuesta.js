const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const encuestaSchema = new Schema({
  titulo: !String,
  opciones: !Array,
  votos: Array,
  descripcion: String
});

const EncuestaModel = mongoose.model("Encuesta", encuestaSchema);

module.exports = EncuestaModel;
