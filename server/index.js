// Dependencias de Express
const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const responseTime = require("response-time");
const cors = require("cors");

// Configuración
const config = require("./config");

// Inicializamos la base de datos / ORM
const mongoose = require("mongoose");
const db = mongoose.connection;

// Inicializamos Express
const app = express();
const api = require("./app/routes");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan("tiny"));
app.use(responseTime());
app.use(cors());

const port = process.env.PORT || config.port;

mongoose.connect(config.db, { useMongoClient: true }, err => {
    if (err) return console.error(`Fallo al conectar al servidor: ${err}`);
});

db.once("open", err => {
    if (err) return console.error(`Fallo al iniciar la base de datos: ${err}`);
    app.listen(port, err => {
        if (err) return console.error(`Error al inicializar Express: ${err}`);
        app.use("/api", api);
        console.log(`Servidor iniciado en el puerto ${port}`);
    });
});